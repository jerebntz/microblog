---
layout: default
permalink: "/wiki/"
---
<section>
<h1>Entries</h1>

<ul>
  {% for entry in site.wiki %}
    <li>
      <a href="{{ entry.url }}">{{ entry.title }}</a>
    </li>
  {% endfor %}
</ul>
</section>
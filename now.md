---
layout: default
permalink: /now/
---
<div class="home"><section>
<h1>Now:</h1>

<ul class="post-list">
    <li>
        <span class="category">Academic:</span> 
        College 
        <time datetime="2015-03-15" class="now-time">Mar 15, 2015</time>
    </li>
    <li>
        <span class="category">Music:</span> 
        Jazz Piano
        <time datetime="2020-01-06" class="now-time">Jun 01, 2020</time>
    </li>
    <li>
        <span class="category">Reading:</span> 
        <a href="https://jerebntz.gitbook.io/projects/accelerated-learning">Accelerated learning</a>
        <time datetime="2020-05-03" class="now-time">May 03, 2020</time>
    </li>
</ul>

<hr>

<h4>May, 2020</h4>
<ul class="post-list">
    <li>
        <span class="category">Academic:</span> 
        College 
        <time datetime="2015-03-15" class="now-time">Mar 15, 2015</time>
    </li>
    <li>
        <span class="category">Reading:</span> 
        <a href="https://jerebntz.gitbook.io/projects/accelerated-learning">Accelerated learning</a>
        <time datetime="2020-05-03" class="now-time">May 03, 2020</time>
    </li>
</ul>
</section></div>

---

## Read
- We Have Always Lived in the Castle by Shirley Jackson <time datetime="2020-04-20" class="now-time">Apr 20, 2020</time> - <time datetime="2020-05-03" class="now-time">May 05, 2020</time>


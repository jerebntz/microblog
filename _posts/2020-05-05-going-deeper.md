---
layout: post
title: Going Deeper
tags: [self]
comment: false
---
> When we give ourselves fewer places to dig, we go deeper, and what we uncover is more rare and valuable than the usual stuff near the surface.
>
> &mdash; David Cain, from [Raptitude](https://www.raptitude.com)

As of right now my [collections](https://bulletjournal.com/blogs/bulletjournalist/collections), have probably more items than what I can go through in a single lifetime, and keep growing in size much faster than I go through them. I believe that algorithm-driven feeds no longer have a purpose for me: I already have as much novelty as I could want (and even more).

This is why I've decided to follow [David's Depth Year tradition](https://www.raptitude.com/2017/12/go-deeper-not-wider/): _"no new hobbies, equipment, games, or books are allowed during this year. Instead, you have to find the value in what you already own or what you’ve already started."_ 

Granted it won't be as _deep_ as David probably envisioned, but it'd be a lot deeper than the status quo. As such, I'm hoping it'll serve as a first step to deeper, more meaningful iterations over the years.

---

I've come up with a simple method for now (to be improved as I go through the project)
#### No social media feeds
These were the ones constantly adding new things to my lists, so I decided to avoid them from now on. This includes:
1. Hiding youtube home feed, subscription tab and related videos column using [DF YouTube](https://addons.mozilla.org/en-US/firefox/addon/df-youtube/)
2. Hiding Home and Local Timeline feeds from [Mastodon](https://merveilles.town/@jere)
3. Not browsing thorugh reddit's r/all nor HackerNews anymore
4. Deleting my blogroll (I might go back to reading blogs in the future, but right now I have enough articles to read, I don't see the need in having more)

I'll still login to those services (except reddit and HN which I quit a while back) to read mentions and messages, and I'll still be reachable through mail. 

#### Working on 1 project per category at a time
The categories are:
1. Academic
2. Engineering
3. Music
4. Painting
5. Passive (consuming books, posts, movies, series, and games)

Each project will have their own wiki page.

#### 1 month quanta
I'll work on each project for at least a month before allowing myself to switch to another one. This of course excludes projects that are finished or enter a __Waiting For__ state before their time slice.

---

I've set a [/now](/now) page, so that anybody interested might take a look from time to time at what I'm working on. Let's see what comes out of this :)

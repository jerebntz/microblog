﻿---
layout: post
title: 2019 Review, 2020 Preview
tags: [self, review]
comment: false
---
# 2019

I lack hard measurements to draw reflections from this year, tracking stuff being one of the things I've been putting off and that I'd like to start doing this 2020. However, here's my year in review, from what I've been feeling this past 12 months:

## The Bad

- My academic performance has been subpar, I could've gotten better results with much less stress
- On a related note, my stress levels have been the highest I can remember, I've gone to bed feeling an unbearable pressure from having to do more things than I could manage several times
- I had a wonky sleep schedule pretty much the whole year, affecting my recovery from training and inducing even more stress (probably also due to being stressed)
- I've deviated from eating healthy more times that I'd like to
- Probably related to the three points above, I've been getting sick a lot compared to previous years

## The Good

- I got my karate black belt. While I still have a lot of things to improve, I'm now more motivated to do so
- I started therapy, which has been of great help in finding out why I felt like I was stuck in the same place
- My life has started moving somewhere. I now feel like I'm moving in circles, but I guess is a normal step when finding your true direction

# 2020

## Low-level goals

- **Intent**: to stop consuming content mindlessly, having algorithms decide what I read, listen to, or watch. Working through my reading, watching and listening lists.
- **Focus**: To stop jumping from subject to subject without learning something of substance first. To give me enough time doing and pursuing things as to reach a deeper level of maturity.
- **Creation**: to stop using the fact that I'm learning as an excuse to avoid putting myself out there. To produce things that people can criticize, helping me learn and improve.
- **Graduation**: to have only exams and my thesis left by the end of 2020.

## Top-level goals and systems

### Ideal daily routine

1. Wake up at 6 am 
2. Cold shower 
3. Brew coffee 
4. Work for 12 pomodoros at up to 3 tasks (giving a max. of 3 pomodoros per task) defined the previous night
5. Lunch (vegan, if possible) for 1 hr
6. Play piano for 2 hrs
7. { Rest of the day }
8. Make dinner and next day's lunch
9. Have dinner for 1 hr
10. Review day, plan next day's tasks and read for at least half an hour

The only thing with an actual hour defined is waking up since the rest of the routine will be adapted to each day according to my obligations and things I'd like to do that day.

### Intent, Focus and Creation

- Stop using the Youtube feed. Work through my playlists, and only add videos needed
  - Only create new playlists for subjects I'm currently learning, and not for those I want to learn in the future
- Stop listening to daily mixes and random playlists on Spotify. Choose 4 albums to listen every month, download them, and leave Spotify in offline mode 
- Stop using Reddit and Instagram compulsively
- Don't scroll through Netflix feed, work on my [to watch](https://trello.com/b/nIjReqnz/movies) list + my netflix list
- Don't read random articles I see linked, work through my [reading list](https://trello.com/b/OUPqlCJp/reading-list)
- When using the pomodoro technique, have a notebook/sheet of paper nearby to jot down ideas that might arise
- Work on projects for at least three months (unless deemed useless) before moving on/abandoning them
- Every day make a list of everything I created
- When I publish something, reward myself

From [Matt D'Avella digital minimalism](https://www.youtube.com/watch?v=5nemvG_EzqY):
1. No screens in bed
2. Schedule all email and instant messages thrice daily
3. Limit social media use to 30 mins daily
4. Limit all video streaming to weekends (except for Youtube tutorials and lessons)

# Conclusion
There are more projects and routines that I want to tackle/incorporate this coming year but they're only tangentially related to my main goals, which is why they're not included here. I want this to reflect the fact that my goals for this year are not, well, goal-oriented, but rather process-oriented. I'm not planning to finish A, nor producing B. I want to, ultimately, change my outlook on life itself, slowly building a mindset that fits what I believe is a good life: **choosing what to pay attention to, being present, leaving the world a different place**, one day at a time.

---
layout: post
title: Días de Cuarentena
tags: [fiction]
comment: false
---

  Por fin se paró el constante ir-y-venir de un grupo que no sabe de dónde viene, ni a dónde va. Que compensa la falta de cercanía con metáforas mal logradas, y la falta de destino con más trabajo, más cosas y menos uno. "Todo lo que te hace falta está dentro tuyo." Claro que esa frase fue dicha en un contexto muy (o no tanto) distinto cuando la escuché, pero estoy casi seguro que es un parafraseo. Que algún maestro, ya olvidado, se la dijo hace mucho a un alumno, jamás recordado, mientras éste miraba su reflejo en un río (o algo igualmente metafórico y zen) y se preguntaba cosas trascendentes que no llegó a responder. Quizás las preguntas eran respuestas a sí mismas. Quizás haya cosas que sea mejor no saber.

  Aunque ahora creo que nos parecemos más al reflejo que a los que lo observan: atrapados, repentinamente conscientes del río que nos sostiene, encierra y sustenta. Pero ¿reflejo de qué? Un espejo que se miente a sí mismo y muestra lo que no está ahí, para sentirse menos solo quizás. No sé, las metáforas a veces se me van de las manos. En cierto sentido las prefiero así: fundamentalmente rotas, a veces bonitas, siempre efímeras. Pero sólo cuando son mías. ¿Cómo podrían atreverse los demás a mostrarme mal lo que prefiero ver bien?

  El maestro y el alumno siguen caminando, dejando el río atrás; ninguno de los dos habla: todo ha sido dicho y cualquier palabra estaría de más.

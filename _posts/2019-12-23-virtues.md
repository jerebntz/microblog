---
layout: post
title: Virtues
tags: [self]
comment: false
---
Inspired by Benjamin Franklin's Thirteen Virtues. None of them are set on stone, the idea is to revisit the list every year.

#### 1. Order and Cleanliness
Of space, body, and mind.

#### 2. Sincerity
To myself and others.

#### 3. Productivity
> "Be always employed in something useful. Cut off all unnecesary distractions."

#### 4. Moderation

#### 5. Creation
End every day having left the world a different place. 

#### 6. Curiosity and Exploration
Learn to love the unknown again.

#### 7. Discipline
> "Perform without fail what you resolve."

#### 8. Excellence
Resolve to perform always as best as possible.

#### 9. Self-Reliance

#### 10. Courage

#### 11. Equilibrium with Nature
Not to take more than I need. To give back whenever I'm able to.

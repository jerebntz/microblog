---
layout: about
title: About
---
# About the blog
### On content
I'd like to write about varied subjects: philosophical/existential ramblings, projects (programming, hardware, others), essays and book reviews. This is by no means a comprehensive list: I might post stuff that won't fit in either of those categories.

### On periodicity
I'll try to post _often_, but I won't push myself to post weekly, nor monthly. I'll write whenever I have something worth writing about.

# About the human
Hello! I'm Jere Benítez, a 24 years old computer engineering student, martial artist and polymath wannabe. 

There's not much to say, because there's not much that's been done (by me). I believe in letting your work speak for you and, so far, I've been pretty silent. Ideally, that will change from now on.

Hope you enjoy my work

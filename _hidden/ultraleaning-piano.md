﻿---
layout: post
title: Ultralearning Piano #1: Metalearning
tags: [ultralearning, music, piano]
comment: false
---

## Introduction
The goal of this project is to be able to play a cover of Ryo Fukui's Scenery one year from now. Half a year after that, I want to play in a live venue, either with a band, or on my own.

Jazz is something that has always been in the back of my head. It started during high school with Miles Davis' Some Kind of Blue, and blossomed during my first year at college with Dave Brubeck's Take Five. From there, I've always been listening at at least one jazz record per month, sometimes going as far as listening exclusively jazz for weeks (Art Blakey's Moanin' is probably one of the songs I've replayed the most last year). Yuki Kodama's Sakamichi no Apollon was also a great inspiration to me, and was the tipping point where I bought a Yamaha keyboard and started taking piano lessons.

However, as I've done with most projects, once it became difficult, once my weaknesses became to obvious to ignore and I felt I no longer could use the 'I'm just starting' excuse (which I still could, but I didn't want to accept that), I started lagging off in my practice. Last Friday was my first lesson after a whole month of not touching my keyboard.

I'm starting this project for various reasons, but mostly because I want to be part of the music that gives me so much satisfaction, that makes me look at life through different eyes. I also want to silence that voice that has always been inside me telling me that I suck at music, that I'll never be able to play an instrument competently.

The probability of me failing is somewhat big: I have a tendency to abandon things that challenge me, and I have absolutely no previous musical instruction nor skill (I can't keep time, nor play things in 8ths for the life of me). Those are the main reasons why this blog exists: to force me to continue, knowing that this is out there where people can read it. That means that I have a kind of "responsibility" to finish this and that I might also get more feedback on my playing, meaning I'll be able to improve faster.

The idea is to share one post per month (meaning the whole project will span almost 18 posts), where I'll share my practice log, my findings, probably some reflection on Ultralearning itself and, after the third month, a small video or audio of me playing so you can provide feedback on things you think I need to drill.

## Roadmap and overview
When I began drafting this post I was thinking about adding a precise roadmap of the whole project (drills, songs, etc), whoever, as I continued reading the book, I realized that the plan itself is not supposed to be that rigid: it should be only rigid enough to give me direction, but flexible enough as to be modified according to what I discover down the road.

In general terms, it can be reduced to three phases:
1. **Meta-music (6 months)**: I'll dedicate these first six months to learn all the music skills a person needs to learn and play proper jazz. According to my research, those skills[^theory] are:
  - ear training.
  - time keeping/rhythm: I know they're two different things, but I'll practice them together.
  - reading music: considering jazz is mostly improvised, this is not as important as it would be if I wanted to play classical music, for instance. However, being able to read standards is important, specially in the beginning, and it'll also help me when reading theory books.

2. **Directness (6 months)**: I took the name of this stage straight out of Young's book. This doesn't mean that directness won't be present in the rest of the stages: I'll always be trying to learn through _direct_ practice. However, the focus of this stage will be in learning songs. While in **Meta-music** I'll be using songs to learn those skills, here I'll be using those skills to play songs. It's obvious from the start that the distinction between these two stages is not clearly defined, but that doesn't really matter for the goal of this project. By the end of this stage, the cover of Scenery will be recorded.

3. **Skin in the game (6 months)**: During this stage, I'll be learning to play with other people. This will mean using a lot of playing along tracks, and going to jams whenever possible. While the concert at the end of this stage might be played on my own, the idea is to learn everything related to "playing live" here, which includes playing with other people too.

The duration of each stage might sound a bit off: stage 1 could only take 3 months, and I could extend stage 2 to 9 months; I could also leave the first stage out entirely and jump straight to stage 2 (and that could be even more in  alignment with the principles in Ultralearning). However I'm giving myself some margin for error here, taking into consideration that this is my first _formal_ project of this nature. Ultimately, those deadlines should be considered more like a placeholder than actual deadlines.

## Updates
### <span id="update1">1st month - 30/10/2019</span>
I failed this month. I've been fighting myself for ever since I can remember, and currently I am not winning the fight. I'm on my way, though. Mini-catharsis aside, I practiced I think 4 or 6 days this month (I have them logged, but I'm afraid to look at it and face my failure). Part of the reason, I think, is that I've been approaching it too academically, which would be OK if I was in an academic setting. 

As a way to fight against it, I've added to the system:
- Jamming with some musician friends once a week. They play in bands and ensembles, but they were talking about having an outlet to play songs they can't play there and I proposed that we played together. I'm a bit scared about exposing myself to others, but I guess that means it's the right thing to do.
- Adding an extra lesson with my piano teacher.
- Getting extra 'lessons' about time and rhythm from a drummer friend. Most likely these won't be weekly, since he's doing it just to help and not charging me or anything.
- Learning to dance. This one is a bit unrelated, but I decided that if I'm going to learn to feel time and rhythm I might as well do it while crossing another thing from my list, and having fun with it. I'll be taking swing lessons for a couple of months, and currently I'm trying to learn reggaeton on my own. No idea where this is going.

The schedule had to be moved around a bit due to exams at college, so the next update will be on the 30th of November.

## Tools

### Ear training
- James Aebersold's Jazz course
- [MusicTheory.net ear training exercises](https://www.musictheory.net/exercises)

// Might be expanded

### Time keeping/rhythm
- [Binary Rhythmic Numerical Alphabet](https://www.youtube.com/watch?v=SfrwBFMBcrw)
- Modern Reading Text in 4/4 by Louis Bellson
- [Barry Harris' Swing By Yourself](https://www.youtube.com/watch?v=RvkFQGyF-l0)

// Might be expanded

### Reading music
- [Music Reading Trainer](https://play.google.com/store/apps/details?id=com.binaryguilt.completemusicreadingtrainer) (Link to google play)

// Might be expanded


# Notes
[^theory]: Theory is not included in the list not because I don't consider it to be a _meta-musical_ skill, but rather because I think that learning it independently of what you're playing (i.e. not as a retrospective analysis of what you just heard or played) is not that useful. As such, it won't be characteristic to this stage, but rather something present in all of them. That being said, I'll be following Mark Levine's Jazz Theory book.

---
layout: page
title: Digital Cleanup
---

Given [a video I saw recently](https://www.youtube.com/watch?v=v3n8txX3144), I've decided to quit Google. It had also occurred to me that some of the apps and services I use can be replaced by vim and some bash scripts, so I decided to tackle both projects at once. As such this is an exercise in minimalism, developing my own tools, and showing the proverbial bird to multinational companies. 

## Cleaning up
The first step I took was to uninstall from my phone all apps that
  1. I couldn't even remember the last time I used them
  2. Made me waste time doing things I didn't really enjoy (i.e. mindless "entertainment")
  3. Had a web app

Of the 57 non-system apps I had on my phone, 13 were uninstalled and 6 were disabled (because I don't have root on my phone yet). 

Then I made a second list of apps to consider whether I'll uninstall, replace, or not touch them. This list was compromised by apps that
  1. Enforced bad habits
  2. I'm not happy with the company behind them
  3. Should had been deleted according to the standards I had set, but I had some reason not to

#### Replaced
  - Google Chrome -> Duck Duck Go, only used to access some university sites in case of an emergency
  - Accuweather -> Forcastie. Accuweather had 13 trackers according to [exodus](https://reports.exodus-privacy.eu.org/en/reports/com.accuweather.android/latest/) (which would explain why it was aporx. 90MBs)

#### Uninstalled/disabled
  - Google Duo
  - Alarmy: 25 trackers, according to [exodus](https://reports.exodus-privacy.eu.org/en/reports/droom.sleepIfUCan/latest/). 


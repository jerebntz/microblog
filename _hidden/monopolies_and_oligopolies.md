---
layout: page
title: Monopolies & Oligopolies
---

# Eyewear
**Held by:** EssilorLuxottica (after the Essilor-Luxottica merger from 1 October 2018)

**Description:** a [vertically integrated](https://en.wikipedia.org/wiki/Vertical_integration) multinational firm, that owns the "design, production and marketing of ophthalmic lenses, optical equipment and prescription glasses and sunglasses."[^essilorluxottica]

**Owns:** (taken from www.essilorluxottica.com/brands on 18 January 2020)

### Lens Technologies
- Crizal
- Essilor
- Essilor Sun Solution
- Eyezen
- Kodak lens
- Oakley
- Optifog
- Ray Ban
- Transitions
- Varilux
- Xperio
- Essilor Instruments
- Satisloh

### Eyewear brands
- alain mikli
- Armani Exchange
- Arnette
- Bolon
- Brooks Brothers
- Burberry
- Bvlgari
- Chanel
- Coach
- Costa
- Dolce & Gabbana
- Emporio Armani
- Scuderia Ferrari
- Foster Grant
- Giorgio Armani
- Luxottica
- Michael Kors
- Miu miu
- Molsion
- Oakley
- Oliver Peoples
- Persol
- Polo by Ralph Lauren
- Prada
- Ralph Lauren
- Ray Ban
- Sferoflex
- Starck eyes
- Tuffany & Co.
- Tory Burch
- Valentino
- Versace
- Vogue

### Direct to consumer
- Clearly
- David Clulow
- Eyebuy Direct
- Eyemed
- Framesdirect.com
- GMO
- Ilori Optical Shop Aspen
- Laubman & Pank Optometrists
- LensCrafters
- LensWay
- MyOptique Group
- Oakley
- Oliver Peoples
- OPSM
- Ópticas Carol
- Pearle Vision
- Persol
- Ray Ban
- Salmoiraghi & Vigano
- Sears Optical
- Sunglass Hut
- Optical
- Vision Direct
- Vogue

[^essilorluxottica]: https://wikipedia.org/wiki/essilorluxottica (Accessed: 18 January 2020)
